package logoparsing;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import logoparsing.LogoParser.AvContext;
import logoparsing.LogoParser.FloatContext;
import logoparsing.LogoParser.ParentheseContext;
import logoparsing.LogoParser.TdContext;

public class LogoTreeVisitor extends LogoBaseVisitor<Integer> {
	Traceur traceur;
	StringProperty log = new SimpleStringProperty();
		
	public LogoTreeVisitor() {
		traceur = new Traceur();
	}
	
    public StringProperty logProperty() {
    	return log;
    }
    
	public Traceur getTraceur() {
		return traceur;
	}

	/*
	 * Map des attributs associés à chaque noeud de l'arbre 
	 * key = node, value = valeur de l'expression du node
	 */
	ParseTreeProperty<Double> atts = new ParseTreeProperty<Double>();

	public void setValue(ParseTree node, double value) {
		atts.put(node, value);
	}

	public double getValue(ParseTree node) {
		Double value = atts.get(node);
		if (value == null) {
			throw new NullPointerException();
		}
		return value;
	}

// Instructions de base	

	@Override
	public Integer visitTd(TdContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {
			traceur.td(bilan._2);
		}
		return 0; 
	}

	@Override
	public Integer visitAv(AvContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {
			traceur.avance(bilan._2);
			log.setValue("Avance de  " + bilan._2);
			log.setValue("\n");
		}
		return bilan._1;
	}

// Expressions

	@Override
	public Integer visitParenthese(ParentheseContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {
			setValue(ctx, bilan._2);
		}
		return bilan._1;
	}

	
	@Override
	public Integer visitFloat(FloatContext ctx) {
		String floatText = ctx.FLOAT().getText();
		setValue(ctx, Double.valueOf(floatText));
		return 0;
	}

	private Binome<Double> evaluate(ParseTree expr) {
		Binome<Double> res = new Binome<>();
		res._1 = visit(expr);
		res._2 = res.isValid() ? getValue(expr) : Double.POSITIVE_INFINITY;
		return res;
	}

	private class Binome<T> {
		public Integer _1;
		public T _2;

		public boolean isValid() {
			return _1 == 0;
		}
	}
}
