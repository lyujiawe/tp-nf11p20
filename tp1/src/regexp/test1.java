package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class test1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		test1();
		System.out.println("--------------test02--------------- ");
		test2();
		System.out.println("--------------test03--------------- ");
		test3();
		System.out.println("--------------test04--------------- ");
		test4();
		System.out.println("--------------test05--------------- ");
		test5();
		System.out.println("--------------test06--------------- ");
//		test6();
		System.out.println("--------------test07--------------- ");
		test7();
		System.out.println("------------test08----------------- ");
		test8();
		System.out.println("--------------test09--------------- ");
		test9();
		System.out.println("-----------------test10---------------- ");
		test10();
		System.out.println("-----------------test2012---------------- ");
		test2012();
		System.out.println("-----------------test2015---------------- ");
		test2015();
	}
	
	public static void test1() {
		String patternString = "^[-+]?[1-9]\\d*|\\b0\\b";
		String text = "007";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		boolean found = m.find(); 
		if (found) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
//			System.out.println("Groupe : " + m.group(1)); 
//			System.out.println("Position fin : " + m.end()); 
//			System.out.println("Après : " + text.substring(m.end()));
		} else {
			System.out.println("No find");
		}
		
	}
	
	public static void test2() {
		String patternString = "^[-+]?((0|[1-9]\\d*)[.])\\d+";
		String text = "101";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		boolean found = m.find(); 
		if (found) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
//			System.out.println("Groupe : " + m.group(1)); 
//			System.out.println("Position fin : " + m.end()); 
//			System.out.println("Après : " + text.substring(m.end()));
		} else {
			System.out.println("No find");
		}
	}
	
	public static void test3() {
		String patternString = "^\\p{Alpha}+[le]+\\p{Alpha}+\\b";
		String text = "1arlement";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		boolean found = m.find(); 
		if (found) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
//				System.out.println("Groupe : " + m.group(1)); 
//				System.out.println("Position fin : " + m.end()); 
//				System.out.println("Après : " + text.substring(m.end()));
		} else {
			System.out.println("No find");
		}
	}
	
	public static void test4() {
		String patternString = "\\b\\w+?(\\d{2,})\\w+";
		String text = "Aee2500By";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		boolean found = m.find(); 
		if (found) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
//			for ( int a : m.group() ){
				System.out.println("Groupe : " + m.group(1)); 
//			}
//				System.out.println("Position fin : " + m.end()); 
//				System.out.println("Après : " + text.substring(m.end()));
		} else {
			System.out.println("No find");
		}
	}
	
	public static void test5() {
		String patternString = "\"args\"\\s*:\\s*\\[(\\d+), (\\d+)\\]";
		String text = "{\"args\":[20, 10]}";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		boolean found = m.find(); 
		if (found) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
			System.out.println("Groupe : " + m.group(1)); 
			System.out.println("Groupe : " + m.group(2));
//				System.out.println("Position fin : " + m.end()); 
//				System.out.println("Après : " + text.substring(m.end()));
		} else {
			System.out.println("No find");
		}
	}
	
	public static void test6() {
		String patternString = "";
		String text = "25.3a512b.3.5135";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		boolean found = m.find(); 
		if (found) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
			while(m.find()) {
				System.out.println("Groupe : " + m.group(1)); 
				System.out.println("Groupe : " + m.group(2));
			}
//				System.out.println("Position fin : " + m.end()); 
//				System.out.println("Après : " + text.substring(m.end()));
		} else {
			System.out.println("No find");
		}
	}
	
	public static void test7() {
		String patternString = "(!i)[b-df-z0-9]+";
		String text = "eiaasod";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		boolean found = m.find(); 
		if (found) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
//			System.out.println("Groupe : " + m.group(1)); 
//			System.out.println("Groupe : " + m.group(2));
//				System.out.println("Position fin : " + m.end()); 
//				System.out.println("Après : " + text.substring(m.end()));
		} else {
			System.out.println("No find");
		}
	}
	
	public static void test8() {
		String patternString = "(!i)[b-df-z0-9]+";
		String text = "eiaasod";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		boolean found = m.find(); 
		if (found) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
//			System.out.println("Groupe : " + m.group(1)); 
//			System.out.println("Groupe : " + m.group(2));
//				System.out.println("Position fin : " + m.end()); 
//				System.out.println("Après : " + text.substring(m.end()));
		} else {
			System.out.println("No find");
		}
	}
	
	public static void test9() {
		String patternString = "[i]";
		String text = "eiaasod";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		boolean found = m.find(); 
		if (found) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
//			System.out.println("Groupe : " + m.group(1)); 
//			System.out.println("Groupe : " + m.group(2));
//				System.out.println("Position fin : " + m.end()); 
//				System.out.println("Après : " + text.substring(m.end()));
		} else {
			System.out.println("No find");
		}
	}
	public static void test10() {
		String patternString = "\\b(\\w)(\\w)\\w*(\\1\\2|\\2\\1)";
		String text = "asxxsa";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		boolean found = m.find(); 
		if (found) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
//			System.out.println("Groupe : " + m.group(1)); 
//			System.out.println("Groupe : " + m.group(2));
//				System.out.println("Position fin : " + m.end()); 
//				System.out.println("Après : " + text.substring(m.end()));
		} else {
			System.out.println("No find");
		}
	}
	
	public static void test2012() {
		String patternString = "(?i)\\b(?=[a-gj-y]*i[a-gj-y]*\\b)(?=\\w*e)[a-gi-y]+\\b";
		String text = "La Gazelle, le Lion, le Hibou, le Singe, le Mistigri, la Licorne.";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		int num = 0; 
		while (m.find()) {
			System.out.println("m =  "+ m.find());
            num++;
        }
        System.out.println("Nombre de sélections= "+ num);
	}
	
	//une chaine de 5 caractères:
	public static void test2015() {
		String patternString = "(?i)\\b\\w([aeiou])\\w\\1\\w\\b|(?!)\\b\\w{2}([aeiou])\\w{2}\\1\\w\\b";
		String text = "lever la tete devant le canal et rester present.";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		int num = 0;
		boolean found = m.find(); 
		while (m.find()) {
			System.out.println("Sélection : " + m.group());
			num++;
		}
		System.out.println("Nombre de sélections= "+ num);
	}
	
	
	
	
	
}
