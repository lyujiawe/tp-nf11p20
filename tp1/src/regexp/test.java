package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("--------------question_1--------------- ");
		question_1();
		System.out.println("--------------question_2---------------- ");
		question_2();
		System.out.println("--------------question_3---------------- ");
		question_3();
		System.out.println("--------------question_4---------------- ");
		question_4();
		System.out.println("--------------question_5---------------- ");
		question_5();
		System.out.println("--------------question_6---------------- ");
		question_6();

	}
	
	public static void question_1() {
		String patternString = "(?i)\\b([b-z]\\w*[b-z])|([a-z]\\w*[b-z])|([b-z]\\w*[a-z])\\b";
		String text = "aab aaa vsfse la";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		while (m.find()) {
//			System.out.println("Position début : " + m.start()); 
//			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
		}
		
	}
	
	public static void question_2() {
		String patternString = "(?i)\\b(\\p{Print}+)\\p{Print}*\\1\\b";
		String text = "absdsab";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		if (m.find()) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
		} else {
			System.out.println("No find");
		}
	}
	
	public static void question_3() {
		String patternString = "(?i)\\b([0-9a-f]{2}:){5}[0-9a-f]{2}\\b";
		String text = "5A:EF:56:A2:AF:18";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		boolean found = m.find(); 
		if (found) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
		} else {
			System.out.println("No find");
		}
	}
	
	public static void question_4() {
		String patternString = "(?i)\\b[0-9]{5}\\w\\b";
		String text = "12345. 12345c";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		if (m.find()) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
		} else {
			System.out.println("No find");
		}
	}
	
	public static void question_5() {
		String text = "nom.prenom@domai111ne.fr";
		String nom = "";
		String prenom = "";
		String domain = "";
		String code = "";
		
		String patternStringNom = "(?i)\\b(?=\\w+.)\\w+\\b";
		Pattern p = Pattern.compile(patternStringNom); 
		Matcher mNom = p.matcher(text); 
		boolean found = mNom.find(); 
		if (found) {
			nom = mNom.group();
			System.out.println("nom : " + nom);
		}
		
		String patternStringPrenom = "(?i)\\b(?=.*@)(?<=.*.)\\w+\\b";
		Pattern pPrenom = Pattern.compile(patternStringPrenom); 
		Matcher mPrenom = pPrenom.matcher(text); 
		found = mPrenom.find(); 
		if (found) {
			prenom = mPrenom.group();
			System.out.println("prenom : " + prenom);
		}
		
		String patternStringDomain = "(?i)\\b(?<=@)[0-9a-z]+\\b";
		Pattern pDomain = Pattern.compile(patternStringDomain); 
		Matcher mDomain = pDomain.matcher(text); 
		found = mDomain.find(); 
		if (found) {
			domain = mDomain.group();
			System.out.println("domain : " + domain);
		}
		
		String patternStringCode = "(?i)\\b(?<=.)\\w{2}\\b";
		Pattern pCode = Pattern.compile(patternStringCode); 
		Matcher mCode = pCode.matcher(text); 
		found = mCode.find(); 
		if (found) {
			code = mCode.group();
			System.out.println("code : " + code);
		}
	}
	
	public static void question_6() {
		String patternString = "\\b(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*\\p{Punct}).*\\b";
		String text = "AXYD+1JBD";
		Pattern p = Pattern.compile(patternString); 
		Matcher m = p.matcher(text); 
		System.out.println("R.E.: " + patternString); 
		System.out.println("Test: " + text);
		boolean found = m.find(); 
		if (found) {
			System.out.println("Position début : " + m.start()); 
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
		} else {
			System.out.println("No find");
		}
	}
	
}
